﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class HandleFinish : MonoBehaviour {


	void OnTriggerEnter(Collider carCollider)
	{
		if (carCollider.tag == "Player") {

			// Write "You Win" to the main screen

			GameObject.Find("SceneController").GetComponent<SceneController>().endGameStatus = "You Win!";


		} else {

			//Write "You Lose" to the main screen

			GameObject.Find("SceneController").GetComponent<SceneController>().endGameStatus = "You Lose!";
		}

		SceneManager.LoadScene("GameEnd");
		Destroy (GameObject.Find ("CompleteGame"));

	}
}
